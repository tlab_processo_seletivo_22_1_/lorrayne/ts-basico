export class Negociacao {
    constructor(_data, //da pra fazer com esse tbm mas tem um buraco, pois só não pode atribuir com =
    _quantidade, //private _quantidade:number, 
    _valor //
    ) {
        this._data = _data;
        this._quantidade = _quantidade;
        this._valor = _valor;
    }
    get data() {
        const data = new Date(this._data.getTime());
        return data;
    }
    get valor() {
        return this._valor;
    }
    get volume() {
        return this._quantidade * this._valor;
    }
}
