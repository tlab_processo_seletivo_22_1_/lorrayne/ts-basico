export class Negociacao{

    constructor(
        private _data: Date, //da pra fazer com esse tbm mas tem um buraco, pois só não pode atribuir com =
        public readonly _quantidade:number, //private _quantidade:number, 
        private _valor:number //
    ){}
    get data(): Date{
        const data = new Date(this._data.getTime());
        return data;
    }  
     get valor(): number{
        return this._valor;
    }  

    get volume(): number{
        return this._quantidade * this._valor;
    }

}